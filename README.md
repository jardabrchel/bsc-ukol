# BSC - Note manager

This project is a note manager made for BSC. 

## Installing project
Go to your workspace directory where you want the app to be installed. Clone the repository by running `git clone git@bitbucket.org:jardabrchel/bsc-ukol.git`. After cloning the repo, go to the project folder by running `cd bsc-ukol`.

## Preparing project
After cloning the repository, you have to install dependent libraries by running `npm i`. After installation of the libraries, you are able to run local server or run the tests.

## Running application

Run `npm start` to launch a dev server. In a browser, navigate to `http://localhost:9000/`. The app will automatically reload if you change any of the source files.

## Running tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

