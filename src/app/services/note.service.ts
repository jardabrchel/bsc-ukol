import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from "@angular/common/http";
import {Note} from "../models/note";
import {API_URL} from "../config/config";

@Injectable({providedIn: 'root'})
export class NoteService {

  constructor(public http: HttpClient,
  ) {
  }

  fetchNotes(): Observable<Note[]> {
    const apiPath = 'notes';
    return this.http.get<Note[]>(API_URL + apiPath);
  }

  fetchNote(id: number): Observable<Note> {
    const apiPath = `notes/${id}`;
    return this.http.get<Note>(API_URL + apiPath);
  }

  addNewNote(title: string): Observable<void> {
    const apiPath = 'notes';
    const apiData = {title};
    return this.http.post<void>(API_URL + apiPath, apiData);
  }

  deleteNote(id: number): Observable<void> {
    const apiPath = `notes/${id}`;
    return this.http.delete<void>(API_URL + apiPath);
  }

  editNote(note: Note): Observable<Note> {
    const apiPath = `notes/${note.id}`;
    const apiData = {title: note.title};
    return this.http.put<Note>(API_URL + apiPath, apiData);
  }

}
