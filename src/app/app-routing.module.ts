import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NoteListComponent} from "./components/note-list/note-list.component";
import {NoteDetailComponent} from "./components/note-detail/note-detail.component";
import {AddNoteComponent} from "./components/add-note/add-note.component";


const routes: Routes = [
  {
    path: '',
    redirectTo: 'note-list',
    pathMatch: 'full',
  },
  {
    path: 'note-list',
    component: NoteListComponent
  },
  {
    path: 'note-detail/:id',
    component: NoteDetailComponent,
  },
  {
    path: 'add-note',
    component: AddNoteComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
