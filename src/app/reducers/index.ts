import {ActionReducerMap, createSelector, MetaReducer} from '@ngrx/store';
import {environment} from '../../environments/environment';
import * as fromNotes from './note.reducer';
import * as fromNoteDetail from './note-detail.reducer';

export interface State {
  notes: fromNotes.NotesState;
  noteDetail: fromNoteDetail.NoteState;
}

export const reducers: ActionReducerMap<State> = {
  notes: fromNotes.reducer,
  noteDetail: fromNoteDetail.reducer,
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

/* Selectors */

export const getNotesState = (state: State) => state.notes;
export const getAllNotes = createSelector(
  getNotesState,
  fromNotes.getAllNotes
);
export const getAllNotesLoading = createSelector(
  getNotesState,
  fromNotes.getAllNotesLoading
);

/* Note detail state */

export const getNoteDetailState = (state: State) => state.noteDetail;
export const getNoteDetail = createSelector(
  getNoteDetailState,
  fromNoteDetail.getNote
);
export const getNoteDetailLoading = createSelector(
  getNoteDetailState,
  fromNoteDetail.getNoteLoading
);

