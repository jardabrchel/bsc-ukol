import * as fromData from '../actions/notes.actions';
import {Note} from '../models/note';

export interface NoteState {
  note: Note;
  loading: boolean;
  error: any;
}

export const initialState: NoteState = {
  note: null,
  loading: false,
  error: null
};

export function reducer(
  state = initialState,
  action: fromData.ActionsUnion
): NoteState {
  switch (action.type) {
    case fromData.ActionTypes.LoadNoteBegin: {
      return {
        ...state,
        loading: true,
        error: null
      };
    }

    case fromData.ActionTypes.LoadNoteSuccess: {
      return {
        ...state,
        loading: false,
        note: action.payload.note
      };
    }

    case fromData.ActionTypes.LoadNoteFailure: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    default: {
      return state;
    }
  }
}

export const getNote = (state: NoteState) => state.note;
export const getNoteLoading = (state: NoteState) => state.loading;
