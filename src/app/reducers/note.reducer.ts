import * as fromData from '../actions/notes.actions';
import {Note} from '../models/note';

export interface NotesState {
  allNotes: Note[];
  loading: boolean;
  error: any;
}

export const initialState: NotesState = {
  allNotes: [],
  loading: false,
  error: null
};

export function reducer(
  state = initialState,
  action: fromData.ActionsUnion
): NotesState {
  switch (action.type) {
    case fromData.ActionTypes.LoadNotesBegin: {
      return {
        ...state,
        loading: true,
        error: null
      };
    }

    case fromData.ActionTypes.LoadNotesSuccess: {
      return {
        ...state,
        loading: false,
        allNotes: action.payload.data
      };
    }

    case fromData.ActionTypes.LoadNotesFailure: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    default: {
      return state;
    }
  }
}

export const getAllNotes = (state: NotesState) => state.allNotes;
export const getAllNotesLoading = (state: NotesState) => state.loading;
