import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap} from 'rxjs/operators';
import {of} from 'rxjs';

import * as DataActions from '../actions/notes.actions';
import {AddNewNoteBegin, DeleteNoteBegin, EditNoteBegin, LoadNoteBegin} from '../actions/notes.actions';
import {NoteService} from '../services/note.service';
import {Router} from '@angular/router';

@Injectable()
export class NotesEffects {
  constructor(
    private actions$: Actions,
    private noteService: NoteService,
    private router: Router,
  ) {
  }

  @Effect()
  loadAllNotes = this.actions$.pipe(
    ofType(DataActions.ActionTypes.LoadNotesBegin),
    switchMap(() => {
      return this.noteService.fetchNotes().pipe(
        map(data => new DataActions.LoadNotesSuccess({data})),
        catchError(error =>
          of(new DataActions.LoadNotesFailure({error}))
        )
      );
    })
  );

  @Effect()
  loadNote = this.actions$.pipe(
    ofType(DataActions.ActionTypes.LoadNoteBegin),
    switchMap((data: LoadNoteBegin) => {
      return this.noteService.fetchNote(data.payload).pipe(
        map(note => new DataActions.LoadNoteSuccess({note})),
        catchError(error =>
          of(new DataActions.LoadNoteFailure({error}))
        )
      );
    })
  );

  @Effect()
  addNewNote = this.actions$.pipe(
    ofType(DataActions.ActionTypes.AddNewNoteBegin),
    switchMap((data: AddNewNoteBegin) => {
      return this.noteService.addNewNote(data.payload).pipe(
        map(() => {
          this.router.navigate(['/']);
          return new DataActions.AddNewNoteSuccess();
        }),
        catchError(error =>
          of(new DataActions.AddNewNoteFailure({error}))
        )
      );
    })
  );

  @Effect()
  deleteNote = this.actions$.pipe(
    ofType(DataActions.ActionTypes.DeleteNoteBegin),
    switchMap((data: DeleteNoteBegin) => {
      return this.noteService.deleteNote(data.payload).pipe(
        map(() => {
          this.router.navigate(['/']);
          return new DataActions.DeleteNoteSuccess();
        }),
        catchError(error =>
          of(new DataActions.DeleteNoteFailure({error}))
        )
      );
    })
  );

  @Effect()
  editNote = this.actions$.pipe(
    ofType(DataActions.ActionTypes.EditNoteBegin),
    switchMap((data: EditNoteBegin) => {
      return this.noteService.editNote(data.payload).pipe(
        map(() => {
          this.router.navigate(['/']);
          return new DataActions.EditNoteSuccess();
        }),
        catchError(error =>
          of(new DataActions.EditNoteFailure({error}))
        )
      );
    })
  );
}
