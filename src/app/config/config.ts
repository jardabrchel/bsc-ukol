export const API_URL = 'https://private-anon-94eac44b62-note10.apiary-mock.com/';

export const DEFAULT_LANGUAGE = 'en';

export const AVAILABLE_LANGS: string[] = [
  'en',
  'cs',
];
