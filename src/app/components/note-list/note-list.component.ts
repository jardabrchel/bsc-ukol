import {Component, OnInit} from '@angular/core';
import {getAllNotes, getAllNotesLoading, State} from '../../reducers';
import {Store} from '@ngrx/store';
import * as NotesActions from '../../actions/notes.actions';
import {Note} from '../../models/note';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss']
})
export class NoteListComponent implements OnInit {
  public notes$: Observable<Note[]>;
  public loading$: Observable<boolean>;

  constructor(
    private store: Store<State>,
  ) { }

  ngOnInit() {
    this.loadNotes();

    this.notes$ = this.store.select(getAllNotes);
    this.loading$ = this.store.select(getAllNotesLoading);
  }

  loadNotes() {
    this.store.dispatch(new NotesActions.LoadNotesBegin());
  }

}
