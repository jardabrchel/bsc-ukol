import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Observable, Subject} from "rxjs";
import {Note} from "../../models/note";
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {getNoteDetail, getNoteDetailLoading, State} from "../../reducers";
import {filter, take, takeUntil} from "rxjs/operators";
import * as NotesActions from '../../actions/notes.actions';

@Component({
  selector: 'app-note-detail',
  templateUrl: './note-detail.component.html',
  styleUrls: ['./note-detail.component.scss']
})
export class NoteDetailComponent implements OnInit, OnDestroy {
  private editNoteForm: FormGroup;
  private noteId: number;
  public loading$: Observable<boolean>;
  private ngDestroyed$ = new Subject();

  constructor(
    private route: ActivatedRoute,
    private store: Store<State>,
  ) { }

  ngOnInit() {
    this.noteId = Number(this.route.snapshot.params['id']);
    this.loadNote();
    this.initEditForm();

    this.store.select(getNoteDetail)
      .pipe(
        filter(note => !!note),
        takeUntil(this.ngDestroyed$),
      ).subscribe((note: Note) => {
        console.log(note);
        this.editNoteForm.get('title').setValue(note.title);
      })
    this.loading$ = this.store.select(getNoteDetailLoading);
  }

  loadNote() {
    this.store.dispatch(new NotesActions.LoadNoteBegin(this.noteId));
  }

  initEditForm() {
    this.editNoteForm = new FormGroup({
      title: new FormControl(null, Validators.required)
    });
  }

  deleteNote() {
    this.store.dispatch(new NotesActions.DeleteNoteBegin(this.noteId));
  }

  onEditNoteSubmit() {
    this.store.dispatch(new NotesActions.EditNoteBegin({
      id: this.noteId,
      title: this.editNoteForm.get('title').value,
    } as Note));
  }

  ngOnDestroy() {
    this.ngDestroyed$.next();
    this.ngDestroyed$.complete();
  }

}
