import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {State} from "../../reducers";
import {Store} from "@ngrx/store";
import * as NotesActions from '../../actions/notes.actions';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss']
})
export class AddNoteComponent implements OnInit {
  private newNoteForm: FormGroup;

  constructor(
    private store: Store<State>,
  ) { }

  ngOnInit() {
    this.newNoteForm = new FormGroup({
      title: new FormControl(null, Validators.required)
    });
  }

  onFormSubmit() {
    this.store.dispatch(new NotesActions.AddNewNoteBegin(this.newNoteForm.get('title').value));
  }

}
