import {Action} from '@ngrx/store';
import {Note} from "../models/note";

export enum ActionTypes {
  LoadNotesBegin = '[ALL NOTES] Load notes begin',
  LoadNotesSuccess = '[ALL NOTES] Load notes success',
  LoadNotesFailure = '[ALL NOTES] Load notes failure',
  AddNewNoteBegin = '[NEW NOTE] Add new note begin',
  AddNewNoteSuccess = '[NEW NOTE] Add new note success',
  AddNewNoteFailure = '[NEW NOTE] Add new note failure',
  DeleteNoteBegin = '[DELETE NOTE] Delete note begin',
  DeleteNoteSuccess = '[DELETE NOTE] Delete note success',
  DeleteNoteFailure = '[DELETE NOTE] Delete note failure',
  LoadNoteBegin = '[LOAD NOTE] Load note begin',
  LoadNoteSuccess = '[LOAD NOTE] Load note success',
  LoadNoteFailure = '[LOAD NOTE] Load note failure',
  EditNoteBegin = '[EDIT NOTE] Edit note begin',
  EditNoteSuccess = '[EDIT NOTE] Edit note success',
  EditNoteFailure = '[EDIT NOTE] Edit note failure',
}

/* Load Notes */
export class LoadNotesBegin implements Action {
  readonly type = ActionTypes.LoadNotesBegin;
}

export class LoadNotesSuccess implements Action {
  readonly type = ActionTypes.LoadNotesSuccess;

  constructor(public payload: { data: Note[] }) {
  }
}

export class LoadNotesFailure implements Action {
  readonly type = ActionTypes.LoadNotesFailure;

  constructor(public payload: { error: any }) {
  }
}

/* Load Note detail */
export class LoadNoteBegin implements Action {
  readonly type = ActionTypes.LoadNoteBegin;

  constructor(public payload: number) {
  }
}

export class LoadNoteSuccess implements Action {
  readonly type = ActionTypes.LoadNoteSuccess;

  constructor(public payload: { note: Note }) {
  }
}

export class LoadNoteFailure implements Action {
  readonly type = ActionTypes.LoadNoteFailure;

  constructor(public payload: { error: any }) {
  }
}

/* Add new note */
export class AddNewNoteBegin implements Action {
  readonly type = ActionTypes.AddNewNoteBegin;

  constructor(public payload: string) {
  }
}

export class AddNewNoteSuccess implements Action {
  readonly type = ActionTypes.AddNewNoteSuccess;
}

export class AddNewNoteFailure implements Action {
  readonly type = ActionTypes.AddNewNoteFailure;

  constructor(public payload: { error: any }) {
  }
}

/* Delete note */
export class DeleteNoteBegin implements Action {
  readonly type = ActionTypes.DeleteNoteBegin;

  constructor(public payload: number) {
  }
}

export class DeleteNoteSuccess implements Action {
  readonly type = ActionTypes.DeleteNoteSuccess;
}

export class DeleteNoteFailure implements Action {
  readonly type = ActionTypes.DeleteNoteFailure;

  constructor(public payload: { error: any }) {
  }
}

/* Edit note */
export class EditNoteBegin implements Action {
  readonly type = ActionTypes.EditNoteBegin;

  constructor(public payload: Note) {
  }
}

export class EditNoteSuccess implements Action {
  readonly type = ActionTypes.EditNoteSuccess;
}

export class EditNoteFailure implements Action {
  readonly type = ActionTypes.EditNoteFailure;

  constructor(public payload: { error: any }) {
  }
}

export type ActionsUnion =
  LoadNotesBegin
  | LoadNotesSuccess
  | LoadNotesFailure
  | LoadNoteBegin
  | LoadNoteSuccess
  | LoadNoteFailure
  | AddNewNoteBegin
  | AddNewNoteSuccess
  | AddNewNoteFailure;
