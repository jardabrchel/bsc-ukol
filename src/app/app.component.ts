import { Component } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {AVAILABLE_LANGS, DEFAULT_LANGUAGE} from "./config/config";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  availableLangs: string[];
  currentLang: string = DEFAULT_LANGUAGE;

  constructor(
    private translate: TranslateService,
  ) {
    this.setAvailableLangs();
    this.translate.setDefaultLang(this.currentLang);
    this.translate.use(this.currentLang);
  }

  setAvailableLangs() {
    this.availableLangs = AVAILABLE_LANGS;
  }

  setLanguage(language: string) {
    this.currentLang = language;
    this.translate.use(language);
  }
}
